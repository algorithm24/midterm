/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Project/Maven2/JavaApp/src/main/java/${packagePath}/${mainClassName}.java to edit this template
 */
package com.pcy.algorithms.ReverseTest;


import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Lenovo
 */
public class Reverse {

    public static void main(String[] args) throws IOException {
        List<String> number = new ArrayList<String>();
        BufferedReader bf = new BufferedReader(new FileReader("C:/Users/Lenovo/OneDrive/เอกสาร/NetBeansProjects/Algorithms/src/main/java/com/pcy/algorithms/ReverseTest/1.in.txt"));
//        Scanner kb = new Scanner(System.in);
        String line = bf.readLine();
        while (line != null) {
            number.add(line);
            line = bf.readLine();
        }
        bf.close();
        String[] array = number.toArray(new String[0]);

//        System.out.println("Enter the numbers");
//        int[] number = new int[4];
        
        

//        for (int i = 0; i < 4; i++) {
//            number[i] = kb.nextInt();
//            
//        }
        long start, stop;
        start = System.nanoTime();
        
        for (int i = 4 - 1; i >= 0; i--) {
            System.out.print(array[i]+" ");
            
        }
        System.out.println("");
        stop = System.nanoTime();
        System.out.println("Time = "+ (stop - start) * 1E-9 + " secs.");
        

    }
}
