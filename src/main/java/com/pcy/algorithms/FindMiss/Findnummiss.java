/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pcy.algorithms.FindMiss;


import java.util.HashMap;
import java.util.Scanner;

public class Findnummiss {

    static int singleNumber(int[] nums, int index) {
        HashMap<Integer, Integer> m = new HashMap<>();
        long sum1 = 0, sum2 = 0;
        for (int i = 0; i < index; i++) {
            if (!m.containsKey(nums[i])) {
                sum1 += nums[i];
                m.put(nums[i], 1);
                
            }
            sum2 += nums[i];
        }
        

        
        return (int) (2 * (sum1) - sum2);
    }

    
    public static void main(String args[]) {
        Scanner kb = new Scanner(System.in);
        System.out.println("Enter the numbers");
        int[] a = new int[7];
         for (int i = 0; i < 7; i++) {
            a[i] = kb.nextInt();
            
        }
        int index = 7;
        System.out.println(singleNumber(a, index));

    }
}

